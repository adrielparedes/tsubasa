package com.rh.tsubasa.utils

import java.security.MessageDigest
import org.apache.commons.codec.digest.DigestUtils
import java.util.Date
import java.text.SimpleDateFormat
import java.util.TimeZone

object DateConverter {
	var datePattern = "MM.dd.HHmm"
	//Convierte a GMT-3
	def convertToFixedTimeZone(date: Date) : String = {
		getFormat().format(date)
	}
  
	def getFormat() : SimpleDateFormat = {
		var format = new SimpleDateFormat(datePattern)
		format.setTimeZone(TimeZone.getTimeZone("GMT-3"))
		format
	}
  
	def isOverdue(fechaPartido : String) : Boolean = {
		var df = new java.text.SimpleDateFormat(datePattern)
		var fechaPartidoDate = new java.util.Date(df.parse(fechaPartido).getTime()-15*60*1000)
		
		var currDateStr = df.format(new java.util.Date());
		
		df.setTimeZone(java.util.TimeZone.getTimeZone("GMT-3"));
		var currDate = df.format(new java.util.Date());
		var serverDateTZ = df.parse(currDate)
		
		return fechaPartidoDate.before(serverDateTZ)
	}

}
package com.rh.tsubasa.utils

import org.apache.logging.log4j.LogManager

trait Loggable {

  var logger = LogManager.getLogger(this)

}
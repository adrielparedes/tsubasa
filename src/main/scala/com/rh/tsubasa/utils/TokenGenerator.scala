package com.rh.tsubasa.utils

import java.security.MessageDigest
import org.apache.commons.codec.digest.DigestUtils

object TokenGenerator {

  val digest = MessageDigest.getInstance("MD5")

  def generate(chain: String) = {
    DigestUtils.shaHex(chain)
  }

}
package com.rh.tsubasa.utils

//import org.json4s.native.Serialization
//import org.json4s.NoTypeHints

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

trait Jsonable[T <: AnyRef] {

  var clazz: Class[T]

  var mapper = new ObjectMapper
  mapper.registerModule(DefaultScalaModule)
  //
  implicit def anyToJson(any: Any): String = mapper.writeValueAsString(any)
  implicit def jsonToType(json: String): T = mapper.readValue(json, clazz)

  //  implicit val formats = Serialization.formats(NoTypeHints)
  //  implicit def anyToJson(any: T): String = Serialization.write(any)
  //  implicit def jsonToType(json: String)(implicit mf: Manifest[T]): T = Serialization.read[T](json)

}
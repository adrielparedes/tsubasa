package com.rh.tsubasa.utils

import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.annotation.security.RolesAllowed
import javax.ws.rs.DELETE
import javax.ws.rs.Consumes
import javax.ws.rs.PathParam
import javax.ws.rs.POST
import javax.ws.rs.HeaderParam

trait CRUDService[M <: AnyRef] extends Jsonable[M] {

  @GET
  @Path("/")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def all: String

  @GET
  @Path("/{id}")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def find(@PathParam("id") id: Int): String

  @POST
  @Path("/new")
  @Produces(Array("application/json"))
  @Consumes(Array("application/json"))
  def create(element: M): M

  @POST
  @Path("/{id}/edit")
  @Produces(Array("application/json"))
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def update(@PathParam("id") id: Int, element: String): String

  @DELETE
  @Path("/{id}")
  @Produces(Array("application/json"))
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def delete(@PathParam("id") id: Int): String

}
package com.rh.tsubasa.utils

import org.infinispan.manager.DefaultCacheManager
import org.infinispan.Cache
import org.infinispan.manager.EmbeddedCacheManager
import org.infinispan.configuration.cache.ConfigurationBuilder
import org.infinispan.eviction.EvictionStrategy._
import org.infinispan.configuration.global.GlobalConfigurationBuilder

object Cache {

  var globalConfig = new GlobalConfigurationBuilder()
    .nonClusteredDefault().globalJmxStatistics().enable().jmxDomain("tsubasa").allowDuplicateDomains(true).build()
  var cacheManager: EmbeddedCacheManager = new DefaultCacheManager(globalConfig)
  cacheManager.defineConfiguration("custom-cache", new ConfigurationBuilder().expiration().lifespan(10 * 60 * 1000)
    .build());

  def getCache: Cache[String, Object] = cacheManager.getCache("custom-cache");

  def stop = cacheManager.getCache().stop()
  def start = cacheManager.getCache().start()

}
package com.rh.tsubasa.services

import javax.ws.rs.Path
import com.rh.tsubasa.utils.Loggable
import com.rh.tsubasa.model.Rank
import com.rh.tsubasa.model.User
import com.rh.tsubasa.model.Rank
import javax.ws.rs.GET
import javax.ws.rs.Produces
import com.rh.tsubasa.model.Reglas
import collection.JavaConversions._
import javax.annotation.security.RolesAllowed
import java.util.HashMap
import java.util.Date
import java.text.SimpleDateFormat
import java.util.TimeZone
import com.rh.tsubasa.utils.DateConverter

@Path("utils")
class UtilsService extends Loggable {

  @GET
  @Path("datetime")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def getDatetime : HashMap[String, String] = {
    
    var map = new HashMap[String, String]
    map.put("value", DateConverter.convertToFixedTimeZone(new Date()))
    return map
  }
  
}
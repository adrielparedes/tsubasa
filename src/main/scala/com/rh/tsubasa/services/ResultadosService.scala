package com.rh.tsubasa.services

import javax.ws.rs.Path
import com.rh.tsubasa.utils.CRUDService
import javax.ws.rs.GET
import javax.ws.rs.Produces
import java.util.Date
import java.util.ArrayList
import com.rh.tsubasa.model.Resultado
import javax.ws.rs.POST
import javax.ws.rs.Consumes
import javax.annotation.security.RolesAllowed
import javax.ws.rs.PathParam
import com.rh.tsubasa.model.User
import com.rh.tsubasa.utils.Cache
import com.rh.tsubasa.utils.Loggable
import javax.ws.rs.QueryParam
import javax.ws.rs.FormParam
import scala.collection.immutable.Map
import java.util.HashMap
import javax.ws.rs.HeaderParam

@Path("resultados")
class ResultadosService extends Loggable {
  var clazz = classOf[Resultado]
  var repository = Resultado
  implicit var mf = manifest[Resultado]
  
  @GET
  @Path("/")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def all: HashMap[String, Resultado] = {
    
    var resultados = Resultado.all.toArray
    var map = new HashMap[String, Resultado]
    resultados.foreach(resultado =>
    	map.put(resultado.partidoID, resultado))
    return map
  }
  
  @POST
  @Path("/")
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("ADMIN"))
  def agregarResultado(@HeaderParam("email") email: String, resultado: Resultado) = {
    logger.info("El usuario " + email + " está agreando el resultado para el partido " + resultado.partidoID)
    var resultDB = Resultado.findBy("partidoID", resultado.partidoID).getOrElse(resultado)
    resultDB.acierto = resultado.acierto
    resultDB.local = resultado.local
    resultDB.visitante = resultado.visitante
    Resultado.save(resultDB)
  }
  
}
package com.rh.tsubasa.services

import javax.ws.rs.Path
import com.rh.tsubasa.utils.CRUDService
import com.rh.tsubasa.model.Role

@Path("roles")
class RoleService extends CRUDService[Role] {

  var clazz = classOf[Role]
  var repository = Role
  implicit var mf = manifest[Role]

  def all: String = repository.all.toList
  def find(id: Int): String = repository.find(id)
  def create(role: Role): Role = {
    //    var role = jsonToType(element)
    repository.save(role)
    role
  }
  def update(id: Int, element: String): String = {
    repository.update(id, element)
  }
  def delete(id: Int): String = repository.find(id)

}
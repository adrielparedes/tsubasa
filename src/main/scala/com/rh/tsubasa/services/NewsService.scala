package com.rh.tsubasa.services

import javax.ws.rs.Path
import javax.ws.rs.GET
import javax.ws.rs.Produces
import com.rh.tsubasa.model.Noticia
import javax.annotation.security.RolesAllowed
import javax.ws.rs.POST
import collection.JavaConversions._
import javax.ws.rs.PathParam
import javax.ws.rs.DELETE
import javax.ws.rs.Consumes
import javax.ws.rs.HeaderParam

@Path("noticias")
class NewsService {

  @GET
  @Path("")
  @Produces(Array("application/json"))
  def allWithoutTribuna: java.util.List[Noticia] = Noticia.allWithoutTribuna.sortBy(_.creationDate).reverse

  @GET
  @Path("all")
  @Produces(Array("application/json"))
  def all: java.util.List[Noticia] = Noticia.all.sortBy(_.creationDate).reverse

  @GET
  @Path("news")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def getNews: java.util.List[Noticia] = Noticia.filterByType("NEWS").sortBy(_.creationDate).reverse

  @GET
  @Path("review")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def getReviews: java.util.List[Noticia] = Noticia.filterByType("REVIEW")

  @POST
  @Path("news")
  @Produces(Array("application/json")) //  @RolesAllowed(Array("ADMIN"))
  def createNews(noticia: Noticia) = Noticia.createNoticia(noticia)

  @GET
  @Path("tribuna")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def getTribuna: java.util.List[Noticia] = Noticia.filterByType("TRIBUNA").sortBy(_.creationDate).reverse

  @POST
  @Path("tribuna")
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def createTribuna(@HeaderParam("email") email: String, noticia: Noticia) = Noticia.createTribuna(email, noticia)

  @POST
  @Path("review")
  @Produces(Array("application/json")) //  @RolesAllowed(Array("ADMIN"))
  def createReview(noticia: Noticia) = Noticia.createReview(noticia)

  @POST
  @Path("{id}/update")
  @Produces(Array("application/json")) //  @RolesAllowed(Array("ADMIN"))
  def update(@PathParam("id") id: Int, noticia: Noticia) = Noticia.update(id, noticia)

  @DELETE
  @Path("{id}")
  @Produces(Array("application/json")) //  @RolesAllowed(Array("ADMIN"))
  def delete(@PathParam("id") id: Int) = Noticia.delete(id)

}
package com.rh.tsubasa.services

import javax.ws.rs.Path
import com.rh.tsubasa.model.User
import com.rh.tsubasa.utils.TokenGenerator
import java.util.HashMap
import javax.ws.rs.WebApplicationException
import javax.ws.rs.POST
import javax.ws.rs.Produces
import javax.ws.rs.Consumes
import org.infinispan.manager.CacheManager
import org.infinispan.manager.DefaultCacheManager
import com.rh.tsubasa.utils.Cache
import java.util.Date
import java.net.HttpURLConnection
import com.rh.tsubasa.utils.Loggable
import com.rh.tsubasa.utils.Jsonable
import com.rh.tsubasa.utils.Jsonable
import java.util.NoSuchElementException
import javax.ws.rs.core.Response
import javax.ws.rs.core.Context
import javax.servlet.http.HttpServletResponse

@Path("login")
class LoginService extends Loggable {

  @POST
  @Path("/")
  @Produces(Array("application/json"))
  @Consumes(Array("application/json"))
  def login(map: java.util.HashMap[String, String]): java.util.HashMap[String, Object] = {

    var email = map.get("email")
    var password = map.get("password")

    logger.info(email)
    logger.info(password)

    try {
      var user = User.findBy("email", email).get
      var encryptedPassword = encrypt(email + password)

      logger.info("Passwords:" + encryptedPassword + " :: " + user.encryptedPassword)

      if (encryptedPassword == user.encryptedPassword) {
        var token: String = generateToken(email + encryptedPassword)
        Cache.getCache.put(email, token)
        Cache.getCache.put("user-" + email, user)
        var response = new java.util.HashMap[String, Object]
        response.put("email", user.getEmail)
        response.put("firstName", user.getFirstName)
        response.put("country", user.getCountry)
        response.put("lastName", user.getLastName)
        response.put("role", user.getRoles)
        response.put("token", token)
        return response
      } else {
        throw new WebApplicationException(HttpURLConnection.HTTP_FORBIDDEN)
      }
    } catch {
      case e: NoSuchElementException => {
        var response = Response.serverError().entity("El usuario: " + email + " no se encuentra registrado").build()
        throw new WebApplicationException(response)
      }
      case e: Throwable => throw new WebApplicationException(e)
    }
  }

  def encrypt(chain: String) = TokenGenerator.generate(chain)
  def generateToken(chain: String) = TokenGenerator.generate(chain + (new Date().getTime()))

}
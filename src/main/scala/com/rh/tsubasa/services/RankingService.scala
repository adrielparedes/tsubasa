package com.rh.tsubasa.services

import javax.ws.rs.Path
import com.rh.tsubasa.utils.Loggable
import com.rh.tsubasa.model.Rank
import com.rh.tsubasa.model.User
import com.rh.tsubasa.model.Rank
import javax.ws.rs.GET
import javax.ws.rs.Produces
import com.rh.tsubasa.model.Reglas
import collection.JavaConversions._
import javax.annotation.security.RolesAllowed

@Path("ranking")
class RankingService extends Loggable {

  @GET
  @Path("")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def getRanking: java.util.List[Rank] = {
    var users = User.allButAdmin
    logger.debug("Usuarios a calcular puntaje: " + users.size);
    var ranking =
      users.map(user => createRank(user)).sortBy(_.puntos).reverse
    ranking.foreach(x => x.setPosition(ranking.indexOf(x) + 1))
    logger.debug("Ranking calculados: " + ranking.size)
    return ranking
  }

  def createRank(user: User) = {
    var rank = new Rank()
    rank.setFirstName(user.getFirstName)
    rank.setLastName(user.getLastName)
    rank.setEmail(user.getEmail)
    rank.setCountry(user.getCountry)
    rank.setPuntos(Reglas.calcularPuntos(user))
    rank
  }

}
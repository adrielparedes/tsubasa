package com.rh.tsubasa.services

import javax.ws.rs.Path
import com.rh.tsubasa.utils.CRUDService
import javax.ws.rs.GET
import javax.ws.rs.Produces
import java.util.Date
import java.util.ArrayList
import com.rh.tsubasa.model.Pronostico
import javax.ws.rs.POST
import javax.ws.rs.Consumes
import javax.annotation.security.RolesAllowed
import javax.ws.rs.PathParam
import com.rh.tsubasa.model.User
import com.rh.tsubasa.utils.Cache
import com.rh.tsubasa.utils.Loggable
import javax.ws.rs.QueryParam
import javax.ws.rs.FormParam
import scala.collection.immutable.Map
import java.util.HashMap
import com.rh.tsubasa.model.Resultado
import javax.ws.rs.HeaderParam
import com.rh.tsubasa.utils.DateConverter
import com.rh.tsubasa.model.Partido

@Path("pronostico")
class PronosticoService extends Loggable {
  var clazz = classOf[Pronostico]
  var repository = Pronostico
  var resultadoRepo = Resultado
  implicit var mf = manifest[Pronostico]

  @GET
  @Path("/")
  @Produces(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def all(@HeaderParam("email") email: String): HashMap[String, Pronostico] = {
    logger.info("All: " + email)
    var user = Cache.getCache.get("user-" + email).asInstanceOf[User]
    var pronosticos = Pronostico.findAllBy("user", user).toArray
    var map = new HashMap[String, Pronostico]
    pronosticos.foreach(pronostico =>
      map.put(pronostico.partidoID, pronostico))
    return map
  }

  @POST
  @Path("/")
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def pronosticar(@HeaderParam("email") email: String, pronostico: Pronostico) = {
    logger.info("Pronosticar: " + email)
//    var resultado = Resultado.findBy("partidoID", pronostico.partidoID)
//    if (resultado == None) //si tiene cargado un resultado, entonces no puede pronosticar
    var partido = Partido.findBy("partidoID", pronostico.partidoID).get
    if(!DateConverter.isOverdue(partido.dia))
    {
      var user = Cache.getCache.get("user-" + email).asInstanceOf[User]
      //      var pronoExistente = Pronostico.findBy("partidoID", pronostico.partidoID).getOrElse(pronostico)
      var pronoExistente = Pronostico.findByUserAndMatch(email, pronostico.partidoID).getOrElse(pronostico)
      pronoExistente.setUser(user)
      pronoExistente.local = pronostico.local
      pronoExistente.empate = pronostico.empate
      pronoExistente.visitante = pronostico.visitante
      pronoExistente.partidoID = pronostico.partidoID
      logger.info(pronoExistente.getId);
      Pronostico.save(pronoExistente)
    } else "No se puede cargar ese pronostico, partido bloqueado" //no hace falta lanzar excepción, dado que solo se daría si hacen un post por fuera de la applicación
  }
}
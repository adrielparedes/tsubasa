package com.rh.tsubasa.services

import scala.collection.JavaConverters._
import com.rh.tsubasa.model.User
import com.rh.tsubasa.utils.CRUDService
import javax.ws.rs.Path
import javax.ws.rs.GET
import javax.ws.rs.Produces
import javax.annotation.security.RolesAllowed
import javax.ws.rs.PathParam
import javax.ws.rs.POST
import javax.ws.rs.Consumes
import com.rh.tsubasa.model.Role
import com.rh.tsubasa.model.exception.RegistrationException
import javax.ws.rs.core.Response
import javax.ws.rs.WebApplicationException
import com.rh.tsubasa.utils.Loggable

@Path("users")
class UserService extends CRUDService[User] with Loggable {

  var clazz = classOf[User]
  var repository = User

  def all: String = repository.all
  def find(id: Int): String = repository.find(id)

  def create(user: User): User = {
    //var user = jsonToType(element)
    try {
      var usrExistente = User.findBy("email", user.getEmail())
      usrExistente match {
        case Some(x) =>
          throw new RuntimeException("Usuario existente")
        case None =>
          repository.save(user)
      }

      user
    } catch {
      case e: Throwable => {
        var response = Response.serverError().entity(e.getMessage()).build()
        throw new WebApplicationException(response)
      }
    }
  }

  def update(id: Int, element: String): String = {
    repository.update(id, element)
  }

  def delete(id: Int): String = repository.find(id)

  @POST
  @Path("/{id}/{role}")
  @Produces(Array("application/json"))
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def addRole(@PathParam("id") id: Int, @PathParam("role") roleId: Int) = {
    var user = repository.find(id)
    var role = Role.find(roleId)
    user.addRole(role)
    repository.update(id, user)
    user
  }

  @GET
  @Path("/{id}/roles")
  @Produces(Array("application/json"))
  @Consumes(Array("application/json"))
  @RolesAllowed(Array("USER"))
  def roles(@PathParam("id") id: Int) = repository.find(id)

}
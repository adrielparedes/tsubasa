package com.rh.tsubasa.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.security.MessageDigest
import com.rh.tsubasa.utils.TokenGenerator
import javax.persistence.Persistence
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import scala.reflect.BeanProperty
import javax.persistence.Table
import javax.persistence.SequenceGenerator
import javax.persistence.Transient
import com.rh.tsubasa.repository.Repository
import javax.persistence.ManyToMany
import collection.JavaConversions._
import com.rh.tsubasa.utils.Jsonable
import javax.persistence.Column
import javax.persistence.ManyToOne
import com.rh.tsubasa.utils.Cache
import com.rh.tsubasa.utils.Loggable
import javax.persistence.FetchType
import javax.persistence.JoinColumn

@Entity
@Table(name = "tsubasa_pronostico")
class Pronostico extends Serializable with Model {

  @BeanProperty
  @ManyToOne
  @JoinColumn(nullable = false)
  var user: User = _

  @BeanProperty
  @Column(nullable = false)
  var partidoID: String = _

  @BeanProperty
  @Column(nullable = false)
  var local: Boolean = _

  @BeanProperty
  @Column(nullable = false)
  var empate: Boolean = _

  @BeanProperty
  @Column(nullable = false)
  var visitante: Boolean = _

}

object Pronostico extends Repository[Pronostico] with Loggable {

  val clazz = classOf[Pronostico]

  def all: List[Pronostico] = {
    var query = entityManager.createQuery(s"SELECT p from Pronostico p")
    var array = query.getResultList().toArray
    array.toList.map(_.asInstanceOf[Pronostico])
  }

  def findAllBy[T](field: String, value: T) = {
    var query = entityManager.createQuery(s"SELECT p from Pronostico p WHERE p.$field = :field")
    var array = query.setParameter("field", value).getResultList().toArray
    array.toList.map(_.asInstanceOf[Pronostico])
  }

  def findBy[T](field: String, value: T): Option[Pronostico] = {
    var query = entityManager.createQuery(s"SELECT p FROM Pronostico p WHERE p.$field = :field ")
    var result: Option[Pronostico] = None
    try {
      result = Some(query.setParameter("field", value).getSingleResult().asInstanceOf[Pronostico])
    } catch { case e: javax.persistence.NoResultException => result = None }
    result
  }

  def findByUserAndMatch[T](email: String, partido: String): Option[Pronostico] = {
    var query = entityManager.createQuery(s"SELECT p FROM Pronostico p WHERE p.partidoID = :partido AND p.user.email = :email")
    var result: Option[Pronostico] = None
    try {
      result =
        Some(
          query.setParameter("partido", partido)
            .setParameter("email", email)
            .getSingleResult().asInstanceOf[Pronostico])
    } catch { case e: javax.persistence.NoResultException => result = None }
    result
  }

}

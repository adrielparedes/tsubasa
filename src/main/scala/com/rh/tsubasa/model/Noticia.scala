package com.rh.tsubasa.model

import scala.beans.BeanProperty
import java.util.Date
import com.rh.tsubasa.repository.Repository
import com.rh.tsubasa.utils.Loggable
import javax.persistence.Entity
import javax.persistence.Column

@Entity
class Noticia extends Model {

  @BeanProperty
  var titulo: String = _

  @BeanProperty
  @Column(columnDefinition = "TEXT")
  var contenido: String = _

  @BeanProperty
  var tipo: String = _

  @BeanProperty
  var image: String = _

  @BeanProperty
  var createdBy: String = _

}

object Noticia extends Repository[Noticia] with Loggable {

  val clazz = classOf[Noticia]

  def all: List[Noticia] = {
    var query = entityManager.createQuery("SELECT u from Noticia u")
    var array = query.getResultList().toArray
    array.toList.map(_.asInstanceOf[Noticia])
  }

  def allWithoutTribuna: List[Noticia] = {
    var query = entityManager.createQuery("SELECT u from Noticia u where u.tipo != 'TRIBUNA'")
    var array = query.getResultList().toArray
    array.toList.map(_.asInstanceOf[Noticia])
  }

  def filterByType(tipo: String): List[Noticia] = {
    var query = entityManager.createQuery("SELECT u from Noticia u where u.tipo = :tipo")
    var array = query.setParameter("tipo", tipo).getResultList().toArray
    logger.debug("Noticias encontradas: " + array.size)
    array.toList.map(_.asInstanceOf[Noticia])
  }

  override def save(noticia: Noticia) = {
    super.save(noticia);
  }

  def createNoticia(noticia: Noticia) = {
    noticia.tipo = "NEWS";
    this.save(noticia);
  }

  def createReview(noticia: Noticia) = {
    noticia.tipo = "REVIEW";
    this.save(noticia);
  }

  def createTribuna(email: String, noticia: Noticia) = {
    var user = User.findBy("email", email).get
    noticia.tipo = "TRIBUNA"
    noticia.createdBy = user.getFirstName + " " + user.getLastName
    this.save(noticia);
  }

  def getTribuna = this.filterByType("TRIBUNA")

}

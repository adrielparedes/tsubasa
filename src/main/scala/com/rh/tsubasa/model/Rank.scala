package com.rh.tsubasa.model

import scala.beans.BeanProperty

class Rank extends Serializable {
  @BeanProperty var position: Int = _
  @BeanProperty var firstName: String = _
  @BeanProperty var lastName: String = _
  @BeanProperty var email: String = _
  @BeanProperty var country: String = _
  @BeanProperty var puntos: Int = _
}
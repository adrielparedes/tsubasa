package com.rh.tsubasa.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.security.MessageDigest
import com.rh.tsubasa.utils.TokenGenerator
import javax.persistence.Persistence
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import scala.reflect.BeanProperty
import javax.persistence.Table
import javax.persistence.SequenceGenerator
import javax.persistence.Transient
import com.rh.tsubasa.repository.Repository
import javax.persistence.ManyToMany
import collection.JavaConversions._
import com.rh.tsubasa.utils.Jsonable
import javax.persistence.Column
import javax.persistence.ManyToOne
import com.rh.tsubasa.utils.Cache
import com.rh.tsubasa.utils.Loggable
import javax.persistence.FetchType

@Entity
@Table(name = "tsubasa_resultado")
class Resultado extends Serializable with Model {

  @BeanProperty
  @Column(nullable = false)
  var partidoID: String = _

  @BeanProperty
  @Column(nullable = false)
  var acierto: String = _
  
  @BeanProperty
  @Column(nullable = false)
  var local: String = _
  
  @BeanProperty
  @Column(nullable = false)
  var visitante: String = _
  

}

object Resultado extends Repository[Resultado] with Loggable {

  val clazz = classOf[Resultado]

  def all: List[Resultado] = {
    var query = entityManager.createQuery(s"SELECT p from Resultado p")
    var array = query.getResultList().toArray
    array.toList.map(_.asInstanceOf[Resultado])
  }
  
  def findBy[T](field: String, value: T) : Option[Resultado] = {
    var query = entityManager.createQuery(s"SELECT p FROM Resultado p WHERE p.$field = :field ")
    var result : Option[Resultado] = None
    try
    {
    result = Some(query.setParameter("field", value).getSingleResult().asInstanceOf[Resultado])
    }
    catch{case e : javax.persistence.NoResultException =>  result = None}
    result
  }
}

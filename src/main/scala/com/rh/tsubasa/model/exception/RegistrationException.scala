package com.rh.tsubasa.model.exception

class RegistrationException(message: String) extends RuntimeException(message) {

}
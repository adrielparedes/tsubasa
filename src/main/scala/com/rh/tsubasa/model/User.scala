package com.rh.tsubasa.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.security.MessageDigest
import com.rh.tsubasa.utils.TokenGenerator
import javax.persistence.Persistence
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import scala.reflect.BeanProperty
import javax.persistence.Table
import javax.persistence.SequenceGenerator
import javax.persistence.Transient
import com.rh.tsubasa.repository.Repository
import javax.persistence.ManyToMany
import collection.JavaConversions._
import com.rh.tsubasa.utils.Jsonable
import javax.persistence.Column
import javax.persistence.NoResultException
import java.util.ArrayList
import com.rh.tsubasa.model.exception.RegistrationException

@Entity
@Table(name = "tsubasa_user")
class User extends Serializable with Model {

  @BeanProperty
  @Column(unique = true, nullable = false)
  var email: String = _

  @BeanProperty
  @Column(nullable = false)
  var firstName: String = _

  @BeanProperty
  @Column(nullable = false)
  var lastName: String = _

  @BeanProperty
  @Column(nullable = false)
  var country: String = _

  @Transient
  @BeanProperty
  var password: String = _

  @Transient
  @BeanProperty
  var passwordConfirmation: String = _

  @JsonIgnore
  @BeanProperty
  @Column(nullable = false)
  var encryptedPassword: String = _

  @BeanProperty
  @ManyToMany
  var roles: java.util.List[Role] = _

  def addRole(role: Role) = {
    if (roles == null) roles = new ArrayList
    roles.add(role)
  }

  def containsRoles(r: List[String]) = roles.exists(x => r.exists(y => x.getRole == y))

}

object User extends Repository[User] {

  val clazz = classOf[User]

  def all: List[User] = {
    var query = entityManager.createQuery("SELECT u from User u")
    var array = query.getResultList().toArray
    array.toList.map(_.asInstanceOf[User])
  }

  override def save(user: User) = {
    if (!isValid(user.getCountry)) throw new RegistrationException("Pais no valido")
    if (user.getPassword != user.getPasswordConfirmation) throw new RegistrationException("Los passwords no concuerdan")
    if (!isValid(user.getEmail) || !user.getEmail.matches(".*@redhat\\.com")) throw new RegistrationException("El email no es valido")
    var encryptedPassword = TokenGenerator.generate(user.email + user.password)
    user.encryptedPassword = encryptedPassword
    var role = Role.findBy("role", "USER").get
    user.addRole(role)
    super.save(user)
  }

  def isValid(string: String) = string != null && string.ne("")

  override def update(id: Int, user: User) = {
    var encryptedPassword = TokenGenerator.generate(user.email + user.password)
    user.encryptedPassword = encryptedPassword
    super.update(id, user)
  }

  def findBy[T](field: String, value: T): Option[User] = {
    var query = entityManager.createQuery(s"SELECT u FROM User u WHERE u.$field = :field ")
    try {
      Some(query.setParameter("field", value).getSingleResult().asInstanceOf[User])
    } catch {
      case e: NoResultException => None
    }
  }

  def allButAdmin = this.all.filterNot(_.getRoles.map(x => x.getRole).contains("ADMIN"))

}

package com.rh.tsubasa.model

import javax.persistence.MappedSuperclass
import javax.persistence.Id
import javax.persistence.GeneratedValue
import scala.reflect.BeanProperty
import javax.persistence.GenerationType
import scala.beans.BeanProperty
import org.hibernate.mapping.OneToMany
import javax.persistence.ManyToOne
import java.util.Date

@MappedSuperclass
trait Model {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @BeanProperty
  var id: Int = _

  @BeanProperty
  var creationDate: Date = _

  @BeanProperty
  var modificationDate: Date = _

}
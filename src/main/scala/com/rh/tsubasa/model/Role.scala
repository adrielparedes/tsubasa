package com.rh.tsubasa.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.rh.tsubasa.repository.Repository
import javax.persistence.Entity
import javax.persistence.Table
import scala.beans.BeanProperty
import javax.persistence.ManyToMany
import javax.persistence.NoResultException

@Entity
@Table(name = "tsubasa_role")
class Role extends Model {

  @BeanProperty
  var role: String = _

  @BeanProperty
  @ManyToMany
  var users: java.util.List[User] = _

}

object Role extends Repository[Role] {

  val clazz = classOf[Role]

  def all: List[Role] = {
    var query = entityManager.createQuery("SELECT u from Role u")
    var array = query.getResultList().toArray
    array.toList.map(_.asInstanceOf[Role])
  }

  def findBy[T](field: String, value: T): Option[Role] = {
    var query = entityManager.createQuery(s"SELECT u FROM Role u WHERE u.$field = :field ")
    try {
      Some(query.setParameter("field", value).getSingleResult().asInstanceOf[Role])
    } catch {
      case e: NoResultException => None
    }
  }

}

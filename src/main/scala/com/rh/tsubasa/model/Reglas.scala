package com.rh.tsubasa.model

import com.rh.tsubasa.utils.Loggable

object Reglas extends Loggable{
  
  def calcularPuntos(user:User) : Int = {
    var aciertos = 0
    var pronosticos = Pronostico.findAllBy("user", user).toArray
    var resultados = Resultado.all
    var partidos = Partido.all
    pronosticos.foreach{pronostico =>
    	var resList = resultados.filter(_.partidoID == pronostico.getPartidoID)
    	if(resList.length > 0)
    	{
    	  var res = resList(0)
    	  if ((res.acierto == "local" && pronostico.local) ||
    	      (res.acierto == "empate" && pronostico.empate) ||
    	      (res.acierto == "visitante" && pronostico.visitante))
    	  {
    		var partido = partidos.filter(_.partidoID == pronostico.getPartidoID)(0).asInstanceOf[Partido]
    	    aciertos+=Integer2int(partido.puntaje)
    	  }
    	}
    }
    logger.info("aciertos: " + aciertos)
	return aciertos
  }

}
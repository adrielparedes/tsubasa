package com.rh.tsubasa.bootstrap

import javax.ws.rs.ext.Provider
import org.jboss.resteasy.annotations.interception.ServerInterceptor
import org.jboss.resteasy.spi.interception.MessageBodyWriterInterceptor
import org.jboss.resteasy.spi.interception.MessageBodyWriterContext
import com.rh.tsubasa.utils.Loggable

@Provider
@ServerInterceptor
class AccessControlInterceptor extends MessageBodyWriterInterceptor with Loggable {

  val ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";

  val ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";

  val ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";

  val ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";

  val ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";

  /**
   * http://www.webdavsystem.com/ajaxfilebrowser/programming/cross_domain
   */

  override def write(context: MessageBodyWriterContext) = {

    logger.warn("Activating cross domain scripting");

    context.getHeaders().add(ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    context.getHeaders().add(ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
    context.getHeaders()
      .add(ACCESS_CONTROL_ALLOW_METHODS,
        "PROPFIND, PROPPATCH, COPY, MOVE, DELETE, MKCOL, LOCK, UNLOCK, PUT, GETLIB, VERSION-CONTROL, CHECKIN, CHECKOUT, UNCHECKOUT, REPORT, UPDATE, CANCELUPLOAD, HEAD, OPTIONS, GET, POST");
    context.getHeaders()
      .add(ACCESS_CONTROL_ALLOW_HEADERS,
        "Overwrite, Destination, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control, email,accept, Authorization");
    context.getHeaders().add(ACCESS_CONTROL_MAX_AGE, "1728000");

    context.proceed();
  }

}

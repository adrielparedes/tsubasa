package com.rh.tsubasa.bootstrap

import javax.servlet.ServletContextListener
import javax.servlet.annotation.WebListener
import javax.servlet.ServletContextEvent
import com.rh.tsubasa.utils.Loggable
import com.rh.tsubasa.utils.Cache
import com.rh.tsubasa.model.Role
import com.rh.tsubasa.model.User
import collection.JavaConversions._

@WebListener
class TsubasaServletContextListener extends ServletContextListener with Loggable {

  def contextDestroyed(event: ServletContextEvent) = {
    Cache.stop
    logger.info("Context Destoyed")
  }

  def contextInitialized(event: ServletContextEvent) = {
    logger.info("Context Initialized")
    Cache.start

    Role.findBy("role", "USER").getOrElse(Role.save(createUser))
    Role.findBy("role", "ADMIN").getOrElse(Role.save(createAdmin))
//    User.findBy("email", "admin@admin").getOrElse(createAdminUser)

  }

  def createUser = {
    logger.info("No existe el ROL USER, se creara")
    var role = new Role
    role.setRole("USER")
    role
  }

  def createAdmin = {
    logger.info("No existe el ROL ADMIN, se creara")
    var role = new Role
    role.setRole("ADMIN")
    role
  }

  def createAdminUser = {

    logger info "No existe el usuario ADMIN, se creara"
    var user = new User
    user.setEmail("admin@admin")
    user.setPassword("admin")
    user.setCountry("World")
    user.setPasswordConfirmation("admin")
    user.setFirstName("Admin")
    user.setLastName("Admin")
    user.addRole(Role.findBy("role", "ADMIN").get)
    User.save(user)

  }

}

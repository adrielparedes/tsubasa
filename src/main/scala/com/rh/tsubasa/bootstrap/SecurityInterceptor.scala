package com.rh.tsubasa.bootstrap

import org.jboss.resteasy.spi.interception.PreProcessInterceptor
import org.jboss.resteasy.spi.HttpRequest
import org.jboss.resteasy.core.ResourceMethodInvoker
import org.jboss.resteasy.core.ServerResponse
import javax.annotation.security.PermitAll
import org.jboss.resteasy.annotations.interception.ServerInterceptor
import javax.annotation.security.RolesAllowed
import javax.ws.rs.ext.Provider
import com.rh.tsubasa.utils.Loggable
import com.rh.tsubasa.model.User
import org.jboss.resteasy.core.Headers
import java.lang.annotation.Annotation
import collection.JavaConversions._
import com.rh.tsubasa.utils.Cache

@Provider
@ServerInterceptor
class SecurityInterceptor extends PreProcessInterceptor with Loggable {

  def rolesAnnotationSearch(annotation: Annotation) = annotation.annotationType().eq(classOf[RolesAllowed])

  def preProcess(httpRequest: HttpRequest, resourceMethodInvoker: ResourceMethodInvoker): ServerResponse = {

    logger.info("Preprocesing")

    var username = getUsername(httpRequest)
    var token = getToken(httpRequest).split("=").last.trim()

    var method = resourceMethodInvoker.getMethod()
    var annotations = resourceMethodInvoker.getMethodAnnotations()

    if (annotations.exists(rolesAnnotationSearch)) {
      var rolesAnnotation = annotations.find(rolesAnnotationSearch).get
      var roles = rolesAnnotation.asInstanceOf[javax.annotation.security.RolesAllowed].value()

      if (!userIsLogged(username, token)) {
        return new ServerResponse("Session Timeout", 419, new Headers[Object]())
      }
      if (!isUserAllowed(username, token, roles)) {
        return new ServerResponse("Unauthorized", 401, new Headers[Object]())
      }
    }

    return null;

  }

  // #####################
  // AUXILIAR
  // #####################

  def isUserAllowed(username: String, token: String, roles: Array[String]): Boolean = {

    var user = User.findBy("email", username).get
    var userToken = Cache.getCache.get(username)

    logger.info("User email: " + user.email)
    user.roles.foreach(x => logger.info("User role: " + x))
    logger.info("Token: " + token)
    logger.info("User Token: " + userToken)

    var sameToken = userToken == token
    var containsRoles = user.containsRoles(roles.toList)
    var isAllowed = sameToken && containsRoles

    logger.info("Is same token? " + sameToken)
    logger.info("Contains Roles? " + containsRoles)
    logger.info(user + " IS ALLOWED? " + isAllowed)

    return isAllowed

  }

  def userIsLogged(username: String, token: String) = Cache.getCache.keys.exists(_ == username)

  def getUsername = getAttribute("email")(_)
  def getToken = getAttribute("Authorization")(_)

  def getAttribute(attribute: String)(httpRequest: HttpRequest) =
    try { httpRequest.getHttpHeaders().getRequestHeader(attribute).get(0) } catch { case e: Exception => "" }

}
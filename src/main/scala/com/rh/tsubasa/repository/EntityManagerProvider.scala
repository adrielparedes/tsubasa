package com.rh.tsubasa.repository

import javax.persistence._
import javax.naming.InitialContext

object EntityManagerProvider {

  //  val entityManagerFactory = Persistence.createEntityManagerFactory("tsubasa")
  //  @PersistenceContext(unitName = "tsubasa")
  //  var entityManager: EntityManager = _
  //  var entityManager: EntityManager = entityManagerFactory.createEntityManager()
  var entityManager: EntityManager = createEntityManagerFactory.createEntityManager()

  def get = entityManager

  def createEntityManagerFactory = new InitialContext().lookup("java:/TsubasaEntityManager").asInstanceOf[EntityManagerFactory]

}

package com.rh.tsubasa.repository

import javax.persistence.Persistence
import com.rh.tsubasa.model.Model
import java.util.Date

trait Repository[T <: Model] {

  val clazz: Class[T]

  var entityManager = EntityManagerProvider.get

  def find(id: Int): T = entityManager.find(clazz, id)

  def all: List[T]

  def save(storable: T) = {
    entityManager.getTransaction().begin()
    storable.creationDate = new Date();
    entityManager.persist(storable)
    entityManager.getTransaction().commit()
    storable
  }

  def update(id: Int, storable: T) = {
    storable.id = id
    entityManager.getTransaction().begin()
    storable.modificationDate = new Date();
    entityManager.merge(storable)
    entityManager.getTransaction().commit()
    storable
  }

  def delete(id: Int) = {
    entityManager.getTransaction().begin()
    var entity = this.find(id)
    entityManager.remove(entity)
    entityManager.getTransaction().commit()
  }
}
  

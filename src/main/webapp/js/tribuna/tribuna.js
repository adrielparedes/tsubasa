'use strict';

/* Controllers */
var mod = angular.module('tribunaModule', ['configurationModule']);

mod.controller('TribunaController', ['$scope', '$route', '$http', 'User',
  '$location', 'Configuration', '$sce', 'DateService',
  function ($scope, $route, $http, User, $location, Configuration, $sce, DateService) {

    $scope.init = function () {
      $scope.refresh();
    };


    $scope.refresh = function () {
      $http.get(Configuration.baseUrl + '/api/rest/noticias/tribuna')
        .success(function (data, status, headers, config) {
          console.log("Posts encontrados: " + data.length);
          $scope.elements = data;
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };

    $scope.create = function (model) {
      console.log(model);
      $http.post(Configuration.baseUrl + '/api/rest/noticias/tribuna', model)
        .success(function (data, status, headers, config) {
          console.log('Creado con exito');
          $scope.refresh();
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };



    $scope.trustAndGetContenido = function (element) {
      return $sce.trustAsHtml(element.contenido);
    };

    $scope.dateService = DateService;

  }]);
'use strict';

var user = angular.module("userModule", []);


user.service("User", function ($window, $rootScope) {

  this.setData = function (data) {

    var json = JSON.stringify(data);
    $window.sessionStorage.data = json;
    $rootScope.$broadcast("user:changed");
  };

  this.getUser = function () {
    var data = $window.sessionStorage.data;

    if (data == undefined) return {};
    else return JSON.parse($window.sessionStorage.data);
  };

  this.getToken = function () {
    return this.getUser().token;
  };

  this.getEmail = function () {
    return this.getUser().email;
  };

  this.getRoles = function () {
    return this.getUser().role;
  };

  this.isLogged = function () {
    var token = this.getToken();
    var isActive = token != undefined && token != "";
    return isActive;
  };

  this.remove = function () {
    delete $window.sessionStorage.data;
  };

  this.validateRoles = function (roles) {
    var userRoles = this.getRoles();
    console.debug('Roles' + JSON.stringify(userRoles));
    if (userRoles == undefined || _.isEmpty(userRoles)) {
      console.debug('El usuario no tiene roles');
      return false;
    }
    return _.some(roles, function (role) {

      var result = _.contains(_.map(userRoles, function (elem) {
        return elem.role
      }), role);
      console.debug('y? ' + result);
      return result;
    });
  };


});
'use strict';

/* Security Module */

var security = angular.module('securityModule', ['userModule']);

security.factory('securityInterceptor', ['$q', 'User', '$location',
  function ($q, User, $location) {
    return {
      request: function (request) {
        request.headers['Content-Type'] = "application/json";
        request.headers['email'] = User.getEmail();
        request.headers['Authorization'] = "Token =" + User.getToken();
        return request;
      },
      responseError: function (response) {
        if (response.status == 419) {
          console.log("Unauthorized");
          var prevRoute = $location.url();
          User.remove()
          $location.path('/login');
          $location.search({
            callback: prevRoute
          });

        }

        if (response.status == 401) {
          console.log("User not Allowed");
          $location.url('/home');

        }

        return $q.reject(response);

      }
    };

}]);
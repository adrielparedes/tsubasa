'use strict';

/* Controllers */
var mod = angular.module('newsModule', ['configurationModule']);

mod.controller('NewsController', ['$scope', '$route', '$http', 'User',
  '$location', 'Configuration', '$sce', 'DateService',
  function ($scope, $route, $http, User, $location, Configuration, $sce, DateService) {

    $scope.init = function (tipo) {
      $http.get(Configuration.baseUrl + '/api/rest/noticias')
        .success(function (data, status, headers, config) {
          console.log("Noticias encontradas: " + data.length);
          $scope.elements = data;
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };


     $scope.trustAndGetContenido = function (element) {
      return $sce.trustAsHtml(element.contenido);
    };

    $scope.dateService = DateService;

  }]);





/**
 *
 *
 *
 *       ADMIN
 *
 *
 */





mod.controller('NewsAdminController', ['$scope', '$route', '$http', 'User',
  '$location', 'Configuration', '$sce','DateService',
  function ($scope, $route, $http, User, $location, Configuration, $sce,DateService) {

    $scope.init = function () {
      $scope.refresh();
    };


    $scope.refresh = function () {
      $http.get(Configuration.baseUrl + '/api/rest/noticias/all')
        .success(function (data, status, headers, config) {
          console.log("Noticias encontradas: " + data.length);
          $scope.elements = data;
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };


    $scope.trustAndGetContenido = function (element) {
      return $sce.trustAsHtml(element.contenido);
    };


    $scope.dateService = DateService;

    $scope.seleccionar = function (element) {
      console.log(element);
      $scope.selected = element;

    };

    $scope.delete = function (selected) {
      $http.delete(Configuration.baseUrl + '/api/rest/noticias/' + selected.id)
        .success(function (data, status, headers, config) {
          console.log('Eliminado con exito');
          $scope.refresh();
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };


    $scope.create = function (selected) {
      console.log("Creando...");
      console.log(selected.tipo);
      $http.post(Configuration.baseUrl + '/api/rest/noticias/' + selected.tipo.toLowerCase(), selected)
        .success(function (data, status, headers, config) {
          console.log('Creado con exito');
          $scope.refresh();
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };

    $scope.update = function (selected) {
      $http.post(Configuration.baseUrl + '/api/rest/noticias/' + selected.id + "/update", selected)
        .success(function (data, status, headers, config) {
          console.log('Updateado con exito');
          $scope.refresh();
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };

  }]);
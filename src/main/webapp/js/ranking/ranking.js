'use strict';

/* Controllers */
var mod = angular.module('rankingModule', ['configurationModule','userModule']);

mod.controller('RankingController', ['$scope', '$route', '$http', '$location', 'Configuration','User',
  function ($scope, $route, $http, $location, Configuration,User) {

    $scope.initialize = function () {
      $http.get(Configuration.baseUrl + '/api/rest/ranking')
        .success(function (data, status, headers, config) {
          $scope.elements = data;
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };
    
    
    $scope.itsMe = function(elem){
      return elem.email == User.getEmail();
    }
  }]);
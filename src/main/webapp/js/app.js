'use strict';

/* App Module */

var tsubasaApp = angular.module('tsubasaApp', [
  'ngRoute',
  'fixtureServices', 'gruposServices',
  'fechasServices', 'pronosticoServices', 'banderasServices', 'utilsServices', 'resultadosServices', 'securityModule', 'loginModule', 'fixtureModule','newsModule','textAngular','resultadosModule',
  'registerModule','tribunaModule', 'applicationModule', 'usersModule', 'configurationModule', 'configurationControllerModule', 'rankingModule', 'angular-loading-bar','ngAnimate']);

tsubasaApp.config(['$routeProvider', '$httpProvider',
  function ($routeProvider, $httpProvider, RestangularProvider) {
    $routeProvider.when('/home', {
      templateUrl: 'partials/home.html',
      active: 'home'
    }).when('/pronostico', {
      templateUrl: 'partials/pronostico.html',
      active: 'pronostico'
    }).when('/login', {
      templateUrl: 'partials/login.html',
      controller: 'LoginController',
      active: 'login'
    }).when('/register', {
      templateUrl: 'partials/register.html',
      controller: 'RegisterController'
    }).when('/ranking', {
      templateUrl: 'partials/ranking.html',
      controller: 'RankingController',
      active: 'ranking'
    }).when('/reglamento', {
      templateUrl: 'partials/reglamento.html',
      active: 'reglamento'
    }).when('/noticias', {
      templateUrl: 'partials/noticias.html',
      controller:'NewsController',
      active: 'noticias'
    }).when('/tribuna', {
      templateUrl: 'partials/tribuna.html',
      controller:'TribunaController',
      active: 'tribuna'
    }).when('/admin/noticias', {
      templateUrl: 'partials/admin/noticias/noticias.html',
      controller:'NewsAdminController'
    }).when('/admin/resultados', {
      templateUrl: 'partials/admin/resultados.html',
      controller:'ResultadosAdminController'
    }).when('/config', {
      templateUrl: 'partials/configuration.html',
      controller: 'ConfigurationController',
      active: 'configuration',
      roles: ['ADMIN']
    }).otherwise({
      redirectTo: '/home'
    });

    $httpProvider.interceptors.push('securityInterceptor');
    $httpProvider.defaults.useXDomain = true;



  }]);

'use strict';

/* Controllers */
var userModule = angular.module('configurationControllerModule', []);

userModule.controller('ConfigurationController', ['$scope', '$route', '$http', '$location',
  function ($scope, $route, $http, $location) {

    //Se fija si tiene los roles asociados
    $scope.authorize();

    $scope.template = "partials/usuarios.html"

    console.debug("hola");
    $scope.show = function (temp) {
      console.debug('click');
      $scope.template = 'partials/' + temp;
    }

  }]);




userModule.service('DateService',
  function () {

    this.dateFormat = 'dd/MM/yyyy HH:mm';

    this.toDate = function (timestamp) {
      return new Date(timestamp);
    };

  });
'use strict';

/* Controllers */
var registerModule = angular.module('registerModule', ['configurationModule']);

registerModule.controller('RegisterController', ['$scope', '$http', 'User',
  '$location', 'Configuration',
  function ($scope, $http, User, $location, Configuration) {

    $scope.register = function () {

      console.debug('Registration');
      console.debug($scope.user);
      $http.post(Configuration.baseUrl + '/' + 'api/rest/users/new', $scope.user)
        .success(function (data, status, headers, config) {
          $location.path('/login');

        }).error(function (data, status, headers, config) {
          $scope.error = data;
          $scope.show = true;
        });
    };

    $scope.signin = function () {
      $location.path('/signin');
    };

    $scope.hide = function () {
      $scope.show = false;
    }


    $scope.countries = ["Argentina", "Perú", "Chile", "Colombia", "México"]


}]);
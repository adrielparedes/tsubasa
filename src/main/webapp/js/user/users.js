'use strict';

/* Controllers */
var userModule = angular.module('usersModule', ['configurationModule']);

userModule.controller('UserController', ['$scope', '$route', '$http', '$location', 'Configuration', 'User',
  function ($scope, $route, $http, $location, Configuration, User) {

    //Se fija si tiene los roles asociados
    $scope.authorize();

    $scope.initialize = function () {
      console.debug("Login: " + Configuration.baseUrl);
      $http.get(Configuration.baseUrl + '/api/rest/users')
        .success(function (data, status, headers, config) {
          $scope.elements = data;
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };
  }]);
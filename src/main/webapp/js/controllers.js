'use strict';

/* Controllers */
var fixtureModule = angular.module('fixtureModule', []);
fixtureModule.controller('FixtureCtrl', ['$scope', '$routeParams', 'Fixture', 'Grupos', 'Fechas', 'Pronostico', 'Resultados', 'Banderas', 'Utils',
 function ($scope, $routeParams, Fixture, Grupos, Fechas, Pronostico, Resultados, Banderas, Utils) {
    $scope.partidos = Fixture.partidos(function (data) {
    	 
      $scope.grupos = Grupos.grupos(function (data) {
        $scope.grupoSeleccionado = data[0];
      });
      
      $scope.pronostico = Pronostico.miPronostico();

      $scope.fechas = Fechas.fechas(function (data) {
        $scope.fechaSeleccionada = data[0];
        $scope.$watch("verPor", function (value) {
          if (value == 'xGrupos') {
            $scope.filterExpr = {
              grupo: $scope.grupos[0].ident
            };
            $scope.grupoSeleccionado = $scope.grupos[0];
          } else {
            $scope.filterExpr = {
              'fecha': $scope.fechas[0].ident
            };
            $scope.fechaSeleccionada = $scope.fechas[0];
          }
        });
      });

      $scope.$watch("grupoSeleccionado", function (value) {
        if (typeof value != 'undefined') {
          $scope.filterExpr = {
            grupo: value.ident
          };
        }
      });
      $scope.$watch("fechaSeleccionada", function (value) {
        if (typeof value != 'undefined') {
          $scope.filterExpr = {
            'fecha': value.ident
          };
        }
      });
      $scope.resultados = Resultados.resultados();
      $scope.verPor = "xFechas";
      $scope.banderas = Banderas.banderas(function (data) {
   
      });
      
      $scope.datetime = Utils.datetime() ;
      $scope.isBlocked = function (datetimePartido, datetimeStr) {
    	  if(typeof datetimeStr != "undefined" && typeof datetimePartido != "undefined")
    	  {
        	  var partidoDate = datetimePartido.split(".");
        	  var partido = new Date();
        	  partido.setMonth(partidoDate[0]); partido.setDate(partidoDate[1]); 
        	  partido.setHours(partidoDate[2].substr(0, 2)); partido.setMinutes(parseInt(partidoDate[2].substr(2, 4))-15);
        	  
        	  var currDatetime = datetimeStr.split(".");
        	  var current = new Date();
        	  current.setMonth(currDatetime[0]); current.setDate(currDatetime[1]); 
        	  current.setHours(currDatetime[2].substr(0, 2)); current.setMinutes(currDatetime[2].substr(2, 4));
        	  
        	  if(partido > current)
        		  	return false
        		  else
        			  return true
    	  } else return true;
      }
      
      $scope.isFaseDeGrupo = function(elem){
        return ['A','B','C','D','E','F','G','H',].indexOf(elem.grupo) != -1;
      };
      
      $scope.getIMG = function (seleccion) {
    	  var img;
    	  for (var i = 0; i < $scope.banderas.length; i++) {
    		  if(typeof $scope.banderas[i][seleccion] != "undefined"){
    			  img = $scope.banderas[i][seleccion];

    		  }	  
    	  }
    	  return img;
      }
      
    });
 }
]);
fixtureModule.controller('PronosticoCtrl', ['$scope', 'Pronostico', 'User',
    function ($scope, Pronostico, User) {

    $scope.setPronostico = function (partidoID, local, empate, visitante) {
      $scope.pronostico[partidoID] = {
        "local": local,
        "empate": empate,
        "visitante": visitante
      };
      Pronostico.pronosticar({
        "partidoID": partidoID,
        "local": local,
        "empate": empate,
        "visitante": visitante
      })
    };
    $scope.calcPronostico = function (partidoID) {
      if ( typeof $scope.resultados[partidoID] == "undefined" || 
    		  (typeof $scope.resultados[partidoID] != "undefined" && $scope.resultados[partidoID].acierto == 'blocked'))
        return 'pendiente';
      else if (
    		  typeof $scope.resultados[partidoID] != "undefined" && typeof $scope.pronostico[partidoID] != "undefined" && (
            ($scope.resultados[partidoID].acierto == 'local' && $scope.pronostico[partidoID].local == true) ||
            ($scope.resultados[partidoID].acierto == 'empate' && $scope.pronostico[partidoID].empate == true) ||
            ($scope.resultados[partidoID].acierto == 'visitante' && $scope.pronostico[partidoID].visitante == true))
      ) {
        return 'correcto';
      } else
        return 'incorrecto';
    };
      
    $scope.parseDate = function(fechaStr)
    {
    	var dateParts = fechaStr.split(".");
    	var dia = dateParts[1];
    	var mes = dateParts[0];
    	var country = User.getUser().country;
    	var hora = parseInt(dateParts[2].substring(0,2));
    	if(country == "Perú" || country == "Colombia" || country == "México")
    		hora = hora - 2;
    	else if(country == "Chile")
    		hora = hora - 1;
    	return dia + "/" + mes + " " + hora + "hs";
    };
    }
]);
'use strict';

/* Services */

var fixtureServices = angular.module('fixtureServices', ['ngResource','configurationModule']);
 
fixtureServices.factory('Fixture', ['$resource', '$location', 'Configuration',
	function($resource, $location, Configuration){
		return $resource(Configuration.baseUrl + '/json/fixture.json', {}, {
			partidos: {method:'GET', isArray:true}
		});
	}
]);

var gruposServices = angular.module('gruposServices', ['ngResource','configurationModule']);
 
gruposServices.factory('Grupos', ['$resource', '$location', 'Configuration',
	function($resource, $location, Configuration){
		return $resource(Configuration.baseUrl + '/json/grupos.json', {}, {
			grupos: {method:'GET', isArray:true}
		});
	}
]);

var fechasServices = angular.module('fechasServices', ['ngResource','configurationModule']);
 
fechasServices.factory('Fechas', ['$resource', '$location', 'Configuration',
	function($resource, $location, Configuration){
		return $resource(Configuration.baseUrl + '/json/fechas.json', {}, {
			fechas: {method:'GET', isArray:true}
		});
	}
]);

var pronosticoServices = angular.module('pronosticoServices', ['ngResource','configurationModule']);
 
pronosticoServices.factory('Pronostico', ['$resource', '$location','Configuration',
	function($resource, $location, Configuration){
		return $resource(Configuration.baseUrl + '/api/rest/pronostico', {}, {
			miPronostico: {method:'GET'},
			pronosticar: {method:'POST'}
		});
	}
]);

var resultadosServices = angular.module('resultadosServices', ['ngResource','configurationModule']);

resultadosServices.factory('Resultados', ['$resource', '$location','Configuration',
	function($resource, $location,Configuration){
		return $resource(Configuration.baseUrl+'/api/rest/resultados', {}, {
			resultados: {method:'GET'}
		});
	}
]);

var banderasServices = angular.module('banderasServices', ['ngResource','configurationModule']);

banderasServices.factory('Banderas', ['$resource', '$location', 'Configuration',
	function($resource, $location, Configuration){
		return $resource(Configuration.baseUrl + '/json/banderas.json', {}, {
			banderas: {method:'GET', isArray:true}
		});
	}
]);

var utilsServices = angular.module('utilsServices', ['ngResource','configurationModule']);

utilsServices.factory('Utils', ['$resource', '$location','Configuration',
	function($resource, $location,Configuration){
		return $resource(Configuration.baseUrl+'/api/rest/utils/datetime', {}, {
			datetime: {method:'GET', isArray:false}
		});
	}
]);


var getPath = function(location){
	return location.absUrl().substr(0, location.absUrl().lastIndexOf("#/"));
}
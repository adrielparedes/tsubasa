'use strict';

/* Controllers */
var mod = angular.module('resultadosModule', ['configurationModule']);

mod.controller('ResultadosAdminController', ['$scope', '$route', '$http', 'User',
  '$location', 'Configuration', '$sce','DateService',
  function ($scope, $route, $http, User, $location, Configuration, $sce,DateService) {

    $scope.init = function () {
      $scope.refresh();
    };


    $scope.refresh = function () {
      $http.get(Configuration.baseUrl + '/api/rest/resultados')
        .success(function (data, status, headers, config) {
          console.log("Resultados encontradas: " + data.length);
          $scope.elements = data;
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };


    $scope.trustAndGetContenido = function (element) {
      return $sce.trustAsHtml(element.contenido);
    };


    $scope.dateService = DateService;

    $scope.seleccionar = function (element) {
      console.log(element);
      $scope.selected = element;
    };


    $scope.create = function (selected) {
      console.log("Creando...");
      console.log(selected.tipo);
      $http.post(Configuration.baseUrl + '/api/rest/resultados', selected)
        .success(function (data, status, headers, config) {
          $scope.refresh();
        }).error(function (data, status, headers, config) {
          console.error(data);
        });
    };

  }]);
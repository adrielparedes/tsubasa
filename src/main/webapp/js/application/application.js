'use strict';

/* Controllers */
var applicationModule = angular.module('applicationModule', ['configurationModule', 'userModule']);

loginModule.controller('ApplicationController', ['$scope', '$route', '$http', 'User',
  '$location', 'Configuration',
  function ($scope, $route, $http, User, $location, Configuration) {

    //Comprueba los roles asociados... llamar explicitamente en los controllers
    $scope.authorize = function () {
      if (!$scope.isAuthorized()) {
        console.debug('Invalid Role');
        $location.path('/forbidden');
      }
    };

    $scope.isAuthorized = function () {
      var roles = $route.current.roles;
      return User.validateRoles(roles);
    };

    $scope.isActive = function (menuItem) {
      if ($route.current == undefined) return false;
      var activeElement = $route.current.active;
      return activeElement == menuItem;
    };

    $scope.user = User;

    $scope.logout = function () {
      console.debug("logout");
      User.remove();
      $location.path('/');
    };

    $scope.canShowElement = function () {
      return User.isLogged();
    };


}]);
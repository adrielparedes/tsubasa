'use strict';

/* Controllers */
var loginModule = angular.module('loginModule', ['configurationModule']);

loginModule.controller('LoginController', ['$scope', '$route', '$http', 'User',
  '$location', 'Configuration',
  function ($scope, $route, $http, User, $location, Configuration) {
    $scope.$route = $route;
    $scope.submit = function () {
      $http.post(Configuration.baseUrl + '/api/rest/login', {
        "email": $("#email").val(),
        "password": $("#password").val()
      }).success(function (data, status, headers, config) {
        User.setData(data);
        var prevRoute = $location.search();
        if (prevRoute == undefined || prevRoute.callback == undefined) {
          $location.path('/');
        } else {
          $location.path(prevRoute.callback);
          $location.search({});
        }

      }).error(function (data, status, headers, config) {
        // Erase the token if the user fails to log in
        console.log(data);
        $scope.error = data;
        $scope.show = true;
        User.remove();
      });
    };


    $scope.hide = function () {
      $scope.show = false;
    }
  }]);